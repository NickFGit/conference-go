from django.views.decorators.http import require_http_methods
from .encoders import AttendeeDetailEncoder, AttendeeEncoder
from django.http import JsonResponse
from .models import Attendee, ConferenceVO
import json


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conferenceVO_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conferenceVO_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conferenceVO_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    if request.method == "GET":
        try:
            attendee = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid attendee id"},
                status=400,
            )
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Attendee.objects.filter(id=id).update(**content)
        location = Attendee.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
