from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO


class ConferenceVOEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVOEncoder(),
    }

    def get_extra_data(self, o):
        has_account = AccountVO.objects.filter(email=o.email).exists()
        return {"has_account": has_account}


class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]
