from conference_go.keys import PEXEL_KEY, OPEN_WEATHER
import requests


def get_city_photo(city):
    response = requests.get(
        url=f"https://api.pexels.com/v1/search?query={city}",
        headers={"Authorization": PEXEL_KEY},
    )
    content = response.json()
    try:
        return content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return None


def get_weather_data(city, state):
    geo_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER,
    }
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geo_url, params=geo_params)
    content = response.json()
    try:
        lattitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": lattitude,
        "lon": longitude,
        "appid": OPEN_WEATHER,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=weather_params)
    content = response.json()
    try:
        description = content["weather"][0]["description"]
        temp = content["main"]["temp"]
    except (KeyError, IndexError):
        return None
    return {"description": description, "temp": temp}
