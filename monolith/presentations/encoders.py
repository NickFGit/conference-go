from .models import Presentation
from common.json import ModelEncoder
from events.encoders import ConferenceEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceEncoder()}

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}
